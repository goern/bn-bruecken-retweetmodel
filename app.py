#!/usr/bin/env python

"""app.py:
"""

import os
import logging

from flask import Flask, jsonify, request
from flask.helpers import make_response
from prometheus_client import Counter, Summary, Histogram, generate_latest, CONTENT_TYPE_LATEST, CollectorRegistry, core
from sklearn.externals import joblib
import pandas as pd


__version__ = '0.12.0-dev'


app = Flask(__name__)

# Lets get our config from ENV
DEBUG_LOG_LEVEL = bool(os.getenv('DEBUG', False))

PREDICTIONS_COUNT = Counter('predictions_total',
                            'Total number of Predictions requested by consumers.')
PREDICTIONS_RETWEET_COUNT = Counter('predictions_retweet_total',
                                    'Total number of Predictions with p >= .95 (retweet) delivered to consumers.')
PREDICTIONS_RESULT = Histogram(
    'prediction_result', 'All the p')
PREDICTIONS_TIME = Histogram('predictions_request_processing_seconds',
                             'DESC: /predict time spent processing request')
FLASK_REQUEST_LATENCY = Histogram(
    'request_latency_seconds', 'Flask Request Latency')


@app.route('/predict', methods=['POST'])
@PREDICTIONS_TIME.time()
def predict():
    json_ = request.json

    predicted = clf.predict([json_['text']])
    PREDICTIONS_COUNT.inc()
    PREDICTIONS_RESULT.observe(predicted)

    retweet = False
    if predicted >= 0.95:
        retweet = True
        PREDICTIONS_RETWEET_COUNT.inc()

    logger.debug({'result': 'ok', 'retweet': retweet,
                  'p': str(predicted[0]), 'text': json_['text']})

    return jsonify({'result': 'ok', 'retweet': retweet, 'p': str(predicted[0]), 'text': json_['text']})


@app.route('/metrics')
@FLASK_REQUEST_LATENCY.time()
def metrics():
    registry = core.REGISTRY
    output = generate_latest(registry)
    response = make_response(output)
    response.headers['Content-Type'] = CONTENT_TYPE_LATEST

    return response


@app.route('/healthz')
@FLASK_REQUEST_LATENCY.time()
def healthz():
    liveliness = {}

    liveliness['OK'] = True

    return jsonify(liveliness), liveliness['OK']


@app.errorhandler(500)
def handle_500(error):
    return str(error), 500


@app.route('/')
def root():
    return jsonify({'result': 'ok'})


if __name__ == '__main__':
    if DEBUG_LOG_LEVEL:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
        urllib3.disable_warnings()

    logger = logging.getLogger(__name__)

    logger.info('initializing...')

    logger.info('loading model...')
    clf = joblib.load('model.pkl')
    logger.info('initialization done!')

    logger.info('starting http server...')
    app.run(host='0.0.0.0', port=8080, debug=1)
